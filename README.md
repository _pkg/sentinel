# sentinel linux package repo  

https://gitlab.com/krink/sentinel    

---

ubuntu 

```    
apt-get update
apt-get install sentinel-runtime
apt-get install senntinel
```    

---

ubuntu 22
/etc/apt/sources.list.d/sentinel.list
```
deb [trusted=yes] https://gitlab.com/_pkg/sentinel/-/raw/master/deb/jammy /
```

ubuntu 20  
/etc/apt/sources.list.d/sentinel.list  
```
deb [trusted=yes] https://gitlab.com/_pkg/sentinel/-/raw/master/deb/focal /  
```

ubuntu 18  
/etc/apt/sources.list.d/sentinel.list  
```
deb [trusted=yes] https://gitlab.com/_pkg/sentinel/-/raw/master/deb/bionic /  
```

ubuntu 16  
/etc/apt/sources.list.d/sentinel.list  
```
deb [trusted=yes] https://gitlab.com/_pkg/sentinel/-/raw/master/deb/xenial /  
```

---


centos  
   
```
yum install sentinel-runtime
yum install sentinel
```

alma 9
/etc/yum.repos.d/sentinel.repo

```
[sentinel]
name = sentinel
baseurl = https://gitlab.com/_pkg/sentinel/-/raw/master/rpm/alma9/x86_64
gpgcheck=0
enabled=1
```


centos 8  
/etc/yum.repos.d/sentinel.repo  

```
[sentinel]
name = sentinel
baseurl = https://gitlab.com/_pkg/sentinel/-/raw/master/rpm/centos8/x86_64
gpgcheck=0
enabled=1
```

centos 7  
/etc/yum.repos.d/sentinel.repo  
```
[sentinel]
name = sentinel
baseurl = https://gitlab.com/_pkg/sentinel/-/raw/master/rpm/centos7/x86_64
gpgcheck=0
enabled=1
```

centos 6  
/etc/yum.repos.d/sentinel.repo  
```
[sentinel]
name = sentinel
baseurl = https://gitlab.com/_pkg/sentinel/-/raw/master/rpm/centos6/x86_64
gpgcheck=0
enabled=1
```


